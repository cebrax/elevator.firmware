/* 
 * File:   main.c
 * Author: abdullah
 *
 * Created on 05 Nisan 2013 Cuma, 10:11
 */

#define _XTAL_FREQ 8000000 // XTAL frequency is 8MHz. This is needed for the __delay_us() function of the XC8.
#include <xc.h> // Include the header file needed by the compiler
#include "software_I2C.h"
#include "DS1307.h"

#define referenceSensor         PORTAbits.RA3
#define referenceSensor_TRIS    TRISAbits.TRISA3
#define button_1stFloor         PORTCbits.RC0
#define button_1stFloor_TRIS    TRISCbits.TRISC0
#define button_2ndFloor         PORTAbits.RA2
#define button_2ndFloor_TRIS    TRISAbits.TRISA2
#define button_3rdFloor         PORTAbits.RA1
#define button_3rdFloor_TRIS    TRISAbits.TRISA1
#define button_4thFloor         PORTAbits.RA0
#define button_4thFloor_TRIS    TRISAbits.TRISA0

#define LED_moving              PORTCbits.RC1
#define LED_moving_TRIS         TRISCbits.TRISC1

#define Stepper_1A              PORTAbits.RA4
#define Stepper_1A_TRIS         TRISAbits.TRISA4
#define Stepper_1B              PORTCbits.RC4
#define Stepper_1B_TRIS         TRISCbits.TRISC4
#define Stepper_2A              PORTCbits.RC3
#define Stepper_2A_TRIS         TRISCbits.TRISC3
#define Stepper_2B              PORTCbits.RC5
#define Stepper_2B_TRIS         TRISCbits.TRISC5

#define BUTTON_TIMEOUT          50

#define STEPPER_DELAY           10       // In terms of milliseconds.
#define FLOOR1_LOCATION         10
#define FLOOR2_LOCATION         110
#define FLOOR3_LOCATION         225
#define FLOOR4_LOCATION         350
#define MAX_STEPS               340

#define FLOOR1_RAM_ADDR         0x10
#define FLOOR2_RAM_ADDR         0x11
#define FLOOR3_RAM_ADDR         0x12
#define FLOOR4_RAM_ADDR         0x13

__CONFIG(FOSC_INTOSCIO & WDTE_OFF & PWRTE_ON & MCLRE_OFF & CP_ON & IOSCFS_8MHZ & BOREN_ON);

/*
 * CONFIGURATION WORD:
 * -------------------
 * Internal Oscillator is set to 8 MHz.
 * I/O function on both of the oscillator pins, i.e on RA4 and on RA5.
 * Master clear is disabled. RA3 pin is digital input.
 * Power-up timer is enabled.
 * Watchdog timer is enabled.
 * Code protection is enabled.
 * Brown-out reset is enabled.
 */
unsigned char stepperSequence = 1;
unsigned int stepperPosition = 0;

unsigned int ctr16bit;

unsigned char leastVisit;
unsigned char mostVisit;
unsigned char mostVisited;
unsigned char floorStatistic_F1;
unsigned char floorStatistic_F2;
unsigned char floorStatistic_F3;
unsigned char floorStatistic_F4;

unsigned char timer_10ms = 0;
unsigned char timer_1sec = 0;

void interrupt myInterrupt(void)
{
    // TMR2 interrupt every 10 ms.
    if (TMR2IF)
    {
        timer_10ms++;
        if (timer_10ms == 100) // 100 * 10ms = 1 second
        {
            timer_1sec++;
            timer_10ms = 0;
        }
        TMR2IF = 0;
    }
}

unsigned char BCDtoDecimal(unsigned char bcd_number)
{
    return ((bcd_number >> 4)*10 + (bcd_number & 0x0F));
}

void setDateTime(void)
{
    /*** Setting RTC Chip ***/

    DS1307_setRegister(DS1307_hours, 0x15);
    DS1307_setRegister(DS1307_minutes, 0x55);
    DS1307_setRegister(DS1307_seconds, 0x30);

    DS1307_setRegister(DS1307_dayOfTheWeek, 0x2);
    DS1307_setRegister(DS1307_dayOfTheMonth, 0x09);
    DS1307_setRegister(DS1307_monthOfTheYear, 0x04);
    DS1307_setRegister(DS1307_year, 0x13);

    DS1307_setRegister(DS1307_configurationByte, 0x00);

    DS1307_setRegister(FLOOR1_RAM_ADDR, 0x00);
    DS1307_setRegister(FLOOR2_RAM_ADDR, 0x00);
    DS1307_setRegister(FLOOR3_RAM_ADDR, 0x01);
    DS1307_setRegister(FLOOR4_RAM_ADDR, 0x00);

    /*** End Of Setting RTC Chip ***/
}

void stepDown(unsigned int steps)
{
    while (steps != 0)
    {
        stepperSequence++;
        if (stepperSequence >= 5) stepperSequence = 1;
        switch (stepperSequence)
        {
        case 1:
            Stepper_1A = 1;
            Stepper_1B = 0;
            Stepper_2A = 1;
            Stepper_2B = 0;
            break;
        case 2:
            Stepper_1A = 1;
            Stepper_1B = 0;
            Stepper_2A = 0;
            Stepper_2B = 1;
            break;
        case 3:
            Stepper_1A = 0;
            Stepper_1B = 1;
            Stepper_2A = 0;
            Stepper_2B = 1;
            break;
        case 4:
            Stepper_1A = 0;
            Stepper_1B = 1;
            Stepper_2A = 1;
            Stepper_2B = 0;
            break;
        }
        steps--;
        __delay_ms(STEPPER_DELAY);
    }
}

void stepUp(unsigned int steps)
{
    while (steps != 0)
    {
        if (stepperSequence <= 1) stepperSequence = 4;
        else stepperSequence--;
        switch (stepperSequence)
        {
        case 1:
            Stepper_1A = 1;
            Stepper_1B = 0;
            Stepper_2A = 1;
            Stepper_2B = 0;
            break;
        case 2:
            Stepper_1A = 1;
            Stepper_1B = 0;
            Stepper_2A = 0;
            Stepper_2B = 1;
            break;
        case 3:
            Stepper_1A = 0;
            Stepper_1B = 1;
            Stepper_2A = 0;
            Stepper_2B = 1;
            break;
        case 4:
            Stepper_1A = 0;
            Stepper_1B = 1;
            Stepper_2A = 1;
            Stepper_2B = 0;
            break;
        }
        steps--;
        __delay_ms(STEPPER_DELAY);
    }
}

void updateStatistics(void)
{
    floorStatistic_F1 = DS1307_readRegister(FLOOR1_RAM_ADDR);
    floorStatistic_F2 = DS1307_readRegister(FLOOR2_RAM_ADDR);
    floorStatistic_F3 = DS1307_readRegister(FLOOR3_RAM_ADDR);
    floorStatistic_F4 = DS1307_readRegister(FLOOR4_RAM_ADDR);

    leastVisit = floorStatistic_F1;
    if (floorStatistic_F2 < leastVisit) leastVisit = floorStatistic_F2;
    if (floorStatistic_F3 < leastVisit) leastVisit = floorStatistic_F3;
    if (floorStatistic_F4 < leastVisit) leastVisit = floorStatistic_F4;

    mostVisit = floorStatistic_F1;
    mostVisited = 1;
    if (floorStatistic_F2 > mostVisit)
    {
        mostVisit = floorStatistic_F2;
        mostVisited = 2;
    }
    if (floorStatistic_F3 > mostVisit)
    {
        mostVisit = floorStatistic_F3;
        mostVisited = 3;
    }
    if (floorStatistic_F4 > mostVisit)
    {
        mostVisit = floorStatistic_F4;
        mostVisited = 4;
    }

    floorStatistic_F1 -= leastVisit;
    floorStatistic_F2 -= leastVisit;
    floorStatistic_F3 -= leastVisit;
    floorStatistic_F4 -= leastVisit;

    DS1307_setRegister(FLOOR1_RAM_ADDR, floorStatistic_F1);
    DS1307_setRegister(FLOOR2_RAM_ADDR, floorStatistic_F2);
    DS1307_setRegister(FLOOR3_RAM_ADDR, floorStatistic_F3);
    DS1307_setRegister(FLOOR4_RAM_ADDR, floorStatistic_F4);
}

void incrementStatistic(unsigned char floorToBeIncremented)
{
    switch (floorToBeIncremented)
    {
    case 1:
        DS1307_setRegister(FLOOR1_RAM_ADDR, (floorStatistic_F1 + 1));
        break;
    case 2:
        DS1307_setRegister(FLOOR2_RAM_ADDR, (floorStatistic_F2 + 1));
        break;
    case 3:
        DS1307_setRegister(FLOOR3_RAM_ADDR, (floorStatistic_F3 + 1));
        break;
    case 4:
        DS1307_setRegister(FLOOR4_RAM_ADDR, (floorStatistic_F4 + 1));
        break;
    }
}

void gotoFloor(unsigned char floorNumber, unsigned char addStatistic)
{
    LED_moving = 1;
    switch (floorNumber)
    {
    case 1:
        if (stepperPosition > FLOOR1_LOCATION)
        {
            stepDown(stepperPosition - FLOOR1_LOCATION);
            stepperPosition = FLOOR1_LOCATION;
        }
        else if (stepperPosition < FLOOR1_LOCATION)
        {
            stepUp(FLOOR1_LOCATION - stepperPosition);
            stepperPosition = FLOOR1_LOCATION;
        }
        if (addStatistic)incrementStatistic(1);
        break;
    case 2:
        if (stepperPosition > FLOOR2_LOCATION)
        {
            stepDown(stepperPosition - FLOOR2_LOCATION);
            stepperPosition = FLOOR2_LOCATION;
        }
        else if (stepperPosition < FLOOR2_LOCATION)
        {
            stepUp(FLOOR2_LOCATION - stepperPosition);
            stepperPosition = FLOOR2_LOCATION;
        }
        if (addStatistic)incrementStatistic(2);
        break;
    case 3:
        if (stepperPosition > FLOOR3_LOCATION)
        {
            stepDown(stepperPosition - FLOOR3_LOCATION);
            stepperPosition = FLOOR3_LOCATION;
        }
        else if (stepperPosition < FLOOR3_LOCATION)
        {
            stepUp(FLOOR3_LOCATION - stepperPosition);
            stepperPosition = FLOOR3_LOCATION;
        }
        if (addStatistic)incrementStatistic(3);
        break;
    case 4:
        if (stepperPosition > FLOOR4_LOCATION)
        {
            stepDown(stepperPosition - FLOOR4_LOCATION);
            stepperPosition = FLOOR4_LOCATION;
        }
        else if (stepperPosition < FLOOR4_LOCATION)
        {
            stepUp(FLOOR4_LOCATION - stepperPosition);
            stepperPosition = FLOOR4_LOCATION;
        }
        if (addStatistic)incrementStatistic(4);
        break;
    }
    updateStatistics();
    timer_1sec = 0;
    LED_moving = 0;
}

void main(void)
{
    __delay_ms(500);

    ANSEL = 0;

    referenceSensor_TRIS = 1;
    button_1stFloor_TRIS = 1;
    button_2ndFloor_TRIS = 1;
    button_3rdFloor_TRIS = 1;
    button_4thFloor_TRIS = 1;

    LED_moving_TRIS = 0;

    Stepper_1A_TRIS = 0;
    Stepper_1B_TRIS = 0;
    Stepper_2A_TRIS = 0;
    Stepper_2B_TRIS = 0;

    T2CON = 0xFF; // Set TMR2 as period register with 1:16 postscaler and 1:16 prescaler.
    PR2 = 77;

    TMR2IE = 1; // Enable interrupts. TMR2 will interrupt every 10 ms.
    PEIE = 1;
    GIE = 1;

    software_I2C_initialize();

    //setDateTime();

    updateStatistics();

    ctr16bit = 0;
    while (referenceSensor && (ctr16bit < MAX_STEPS))
    {
        stepDown(1);
        ctr16bit++;
    }

    while (!referenceSensor)
    {
        stepUp(1);
        stepperPosition++;
    }

    if ((DS1307_readRegister(DS1307_hours) < 0x08) || (DS1307_readRegister(DS1307_hours) > 0x15))
        gotoFloor(1, 0);
    else
        gotoFloor(mostVisited, 0);

    while (1)
    {
        if (button_1stFloor)
        {
            __delay_ms(BUTTON_TIMEOUT);
            if (button_1stFloor && button_2ndFloor)
            {
                __delay_ms(2000);
                if (button_1stFloor && button_2ndFloor)
                {
                    DS1307_setRegister(FLOOR1_RAM_ADDR, 0x01);
                    DS1307_setRegister(FLOOR2_RAM_ADDR, 0x00);
                    DS1307_setRegister(FLOOR3_RAM_ADDR, 0x00);
                    DS1307_setRegister(FLOOR4_RAM_ADDR, 0x00);
                    updateStatistics();
                    LED_moving = 1;
                    __delay_ms(500);
                    LED_moving = 0;
                    __delay_ms(500);
                    LED_moving = 1;
                    __delay_ms(500);
                    LED_moving = 0;
                    gotoFloor(mostVisited, 0);
                }
            }
            else if (button_1stFloor)
            {
                gotoFloor(1, 1);
            }
        }
        if (button_2ndFloor)
        {
            __delay_ms(BUTTON_TIMEOUT);
            if (button_1stFloor && button_2ndFloor)
            {
                __delay_ms(2000);
                if (button_1stFloor && button_2ndFloor)
                {
                    DS1307_setRegister(FLOOR1_RAM_ADDR, 0x01);
                    DS1307_setRegister(FLOOR2_RAM_ADDR, 0x00);
                    DS1307_setRegister(FLOOR3_RAM_ADDR, 0x00);
                    DS1307_setRegister(FLOOR4_RAM_ADDR, 0x00);
                    updateStatistics();
                    LED_moving = 1;
                    __delay_ms(500);
                    LED_moving = 0;
                    __delay_ms(500);
                    LED_moving = 1;
                    __delay_ms(500);
                    LED_moving = 0; 
                    gotoFloor(mostVisited, 0);
                }
            }
            else if (button_2ndFloor)
            {
                gotoFloor(2, 1);
            }
        }
        if (button_3rdFloor)
        {
            __delay_ms(BUTTON_TIMEOUT);
            if (button_3rdFloor)
            {
                gotoFloor(3, 1);
            }
        }
        if (button_4thFloor)
        {
            __delay_ms(BUTTON_TIMEOUT);
            if (button_4thFloor)
            {
                gotoFloor(4, 1);
            }
        }
        if (timer_1sec > 1)
        {
            if (Stepper_1A)
            {
                Stepper_1A = ~Stepper_1A;
                __delay_us(80);
                Stepper_1A = ~Stepper_1A;
            }
            if (Stepper_1B)
            {
                Stepper_1B = ~Stepper_1B;
                __delay_us(80);
                Stepper_1B = ~Stepper_1B;
            }
            if (Stepper_2A)
            {
                Stepper_2A = ~Stepper_2A;
                __delay_us(80);
                Stepper_2A = ~Stepper_2A;
            }
            if (Stepper_2B)
            {
                Stepper_2B = ~Stepper_2B;
                __delay_us(80);
                Stepper_2B = ~Stepper_2B;
            }
        }
        if (timer_1sec >= 20)
        {
            if ((DS1307_readRegister(DS1307_hours) < 0x08) || (DS1307_readRegister(DS1307_hours) > 0x17))
                gotoFloor(1, 0);
            else
                gotoFloor(mostVisited, 0);
        }
    }
}
