/* 
 * File:   software_I2C.h
 * Author: abdullah
 *
 * Created on 19 Aralık 2012 Çarşamba, 14:49
 */

#ifndef SOFTWARE_I2C_H
#define	SOFTWARE_I2C_H

#ifndef I2C_SDA
#define I2C_SDA PORTCbits.RC2
#endif

#ifndef I2C_SCL
#define I2C_SCL PORTAbits.RA5
#endif

#ifndef I2C_SDA_TRIS
#define I2C_SDA_TRIS TRISCbits.TRISC2
#endif

#ifndef I2C_SCL_TRIS
#define I2C_SCL_TRIS TRISAbits.TRISA5
#endif

void software_I2C_startCondition(void);
void software_I2C_stopCondition(void);
void software_I2C_sendBit(unsigned char bitToBeSent);
bit software_I2C_readBit(void);
bit software_I2C_sendByte(unsigned char byteToBeSent);
unsigned char software_I2C_readByte(unsigned char nack);
void software_I2C_initialize(void);

#endif	/* SOFTWARE_I2C_H */

