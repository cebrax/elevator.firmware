#ifndef _XTAL_FREQ
#define _XTAL_FREQ 8000000 // XTAL frequency is 8MHz. This is needed for the __delay_us() function of the XC8.
#endif
#define I2C_Half_Clock_Period   50
#define I2C_Clock_Period    100
#include "software_I2C.h"
#include <xc.h> // Include the header file needed by the compiler
bit software_I2C_started;

void software_I2C_startCondition(void)
{
    if (software_I2C_started == 1)
    {
        I2C_SCL = 0;
        __delay_us(I2C_Half_Clock_Period);
        I2C_SDA = 1;
        __delay_us(I2C_Half_Clock_Period);
    }
    I2C_SCL = 1;
    __delay_us(I2C_Half_Clock_Period);

    I2C_SDA = 0;
    __delay_us(I2C_Half_Clock_Period);

    software_I2C_started = 1;
}

void software_I2C_stopCondition(void)
{
    I2C_SCL = 0;
    __delay_us(I2C_Half_Clock_Period);
    I2C_SDA = 0;
    __delay_us(I2C_Half_Clock_Period);

    I2C_SCL = 1;
    __delay_us(I2C_Half_Clock_Period);
    I2C_SDA = 1;
    __delay_us(I2C_Half_Clock_Period);
    software_I2C_started = 0;
}

void software_I2C_sendBit(unsigned char bitToBeSent)
{
    I2C_SCL = 0;
    __delay_us(I2C_Half_Clock_Period);
    I2C_SDA = 0x01 && (bitToBeSent & 0x01);
    __delay_us(I2C_Half_Clock_Period);
    I2C_SCL = 1;
    __delay_us(I2C_Clock_Period);
}

bit software_I2C_readBit(void)
{
    static bit readValue;
    I2C_SCL = 0;
    __delay_us(5);
    I2C_SDA_TRIS = 1;
    __delay_us(I2C_Clock_Period);
    I2C_SCL = 1;
    __delay_us(I2C_Half_Clock_Period);
    readValue = I2C_SDA;
    __delay_us(I2C_Half_Clock_Period);
    I2C_SDA_TRIS = 0;
    return readValue;
}

bit software_I2C_sendByte(unsigned char byteToBeSent)
{
    static unsigned char counter;
    static unsigned char mask;
    mask = 0x80;
    for (counter = 0; counter < 8; counter++)
    {
        if (mask == (byteToBeSent & mask))
        {
            software_I2C_sendBit(1);
        }
        else
        {
            software_I2C_sendBit(0);
        }
        mask >> = 1;
    }
    return software_I2C_readBit();
}

unsigned char software_I2C_readByte(unsigned char nack)
{
    static unsigned char counter;
    static unsigned char buffer;
    buffer = 0;
    for (counter = 0; counter < 8; counter++)
    {
        buffer <<= 1;
        buffer |= software_I2C_readBit();
    }
    if (nack)software_I2C_sendBit(1);
    else software_I2C_sendBit(0);
    return buffer;
}

void software_I2C_initialize(void)
{
    I2C_SCL_TRIS = 0;
    I2C_SDA_TRIS = 0;
    __delay_us(5);
    I2C_SCL = 1;
    __delay_us(5);
    I2C_SDA = 1;
    __delay_ms(10);
    software_I2C_started = 0;
}