/* 
 * File:   DS1307.h
 * Author: abdullah
 *
 * Created on 09 Ocak 2013 Çarşamba, 11:32
 */

#ifndef DS1307_H
#define	DS1307_H

#define DS1307_seconds              0
#define DS1307_minutes              1
#define DS1307_hours                2
#define DS1307_dayOfTheWeek         3
#define DS1307_dayOfTheMonth        4
#define DS1307_monthOfTheYear       5
#define DS1307_year                 6
#define DS1307_configurationByte    7

void DS1307_setRegister(unsigned char registerToBeSet, unsigned char valueToApply);
unsigned char DS1307_readRegister(unsigned char registerToRead);

#endif	/* DS1307_H */

