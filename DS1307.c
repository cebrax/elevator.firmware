#include "software_I2C.h"

void DS1307_setRegister(unsigned char registerToBeSet, unsigned char valueToApply)
{
    software_I2C_startCondition();
    software_I2C_sendByte(0xD0);
    software_I2C_sendByte(registerToBeSet);
    software_I2C_sendByte(valueToApply);
    software_I2C_stopCondition();
}

unsigned char DS1307_readRegister(unsigned char registerToRead)
{
    static unsigned char result = '\0';
    software_I2C_startCondition();
    software_I2C_sendByte(0xD0);
    software_I2C_sendByte(registerToRead);
    software_I2C_startCondition();
    software_I2C_sendByte(0xD1);
    result = software_I2C_readByte(1);
    software_I2C_stopCondition();
    return result;
}
